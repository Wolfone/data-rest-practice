### Important notice:

* This project uses Lombok (https://projectlombok.org/).
Please note that some IDEs might need a plugin and/or annotation-preprocessing
turned on to not show errors that would be resolved through code-generation
at compile-time.

* Furthermore this project runs with an in memory H2 database by default. If you wanna switch to for example
mysql an example-application.properties file is included in this repo as well as a
commented-out dependency in the pom. Remark that application.properties contains
a DB password. This is ONLY for the sake of the example!
**Never push unencrypted relevant credentials to a repo.**

* this project uses maven and packages to a war which can be deployed to a suitable 
application-server (e.g. tomcat9)

* "on the fly setup" (with server included) can be done via:
    
    mvn spring-boot:run
    
* exposed endpoints: (POST) "/person/create", (PUT) "/person/delete", (DELETE) "/person/update"
* expected payload-format is JSON featuring the keys: 
    - *firstName* String of length l with 0 <= l <= 20
    - *lastName* String of length l with 3 <= l <=20
    - *dateOfBirth* YYYY-MM-DD

* example call to the "on the fly"-served app (should return 200 with the created person-data as payload): 

    curl --location --request POST 'localhost:8080/person/create' \
    --header 'Content-Type: application/json' \
    --header 'Content-Type: text/plain' \
    --data-raw '{
    	"firstName": "Max",
    	"lastName": "Mustermann",
    	"dateOfBirth": "2000-01-01"
    }'

#### caveats

* remark that the provided integration-test does test the response-status-code but
NOT the actual existence of the entry in the DB (
the reasoning behind this based on my current knowledge is as follows: to do this either the repository would
have to be public, which seems like a design-flaw when using feature-oriented
package structure OR the service would have to expose a public (tested) read-method) 
  
### if (yourMemoryFailsYou(person)):

* Spring Data REST - Reference: 
    https://docs.spring.io/spring-data/rest/docs/current/reference/html/#reference
* Spring Data Application Properties - Reference:
    https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#data-properties

### Somewhat related things that came up during research but are not particularly context-important: 

* Robert C. Martin about architecture: 
    https://www.youtube.com/watch?v=Nsjsiz2A9mg&feature=youtu.be
    / https://www.youtube.com/watch?v=2dKZ-dWaCiU
 
* Entry-point **feature oriented software development**-research:
    http://fosd.net/
package bous.philipp.datarestpractice.person;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class PersonIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    @Description("Post-request to /person/create with valid payload should return status-code 200.")
    void createPersonValidReturnCode200() {
        try {
            mockMvc.perform(post("/person/create").contentType("application/json").content("{\n" +
                    "\t\"firstName\": \"Max\",\n" +
                    "\t\"lastName\": \"Mustermann\",\n" +
                    "\t\"dateOfBirth\": \"2000-01-01\"\n" +
                    "}")
            ).andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

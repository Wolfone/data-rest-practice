package bous.philipp.datarestpractice.person;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Import;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


@Import(PersonObjectServiceTestConfiguration.class)
@SpringBootTest
public class PersonObjectServiceTest {

    @Autowired
    PersonObjectService service;

    @MockBean
    PersonObjectRepository repository;


    @Test
    @Description("Trying to create a person with an already existing ID should throw ")
    void createPersonIdAlreadyExists() {
        PersonObject person = initiallyValidPerson();

        Mockito.when(repository.existsById(1L)).thenReturn(true);

        assertThatThrownBy(() -> {
            service.createPerson(person);
        }).isInstanceOf(PersonObjectIdAlreadyExistsException.class);
    }

    @Test
    @Description("Trying to create a (otherwise valid) person with date of birth in future should throw IllegalPersonObjectDataException.")
    void createPersonTooYoung() {
        PersonObject person = initiallyValidPerson();
        person.setDateOfBirth(LocalDate.of(2021, 1, 1));

        Mockito.when(repository.save(person)).thenReturn(person);

        assertThatThrownBy(() -> {
            service.createPerson(person);
        }).isInstanceOf(IllegalPersonObjectDataException.class);
    }

    @Test
    @Description("Trying to create a (otherwise valid) person with a last name exceeding 20 characters should throw IllegalPersonObjectDataException.")
    void createPersonTooLongLastName() {
        PersonObject person = initiallyValidPerson();
        person.setLastName("FriedhelmMeyerVonDerHeide");

        Mockito.when(repository.save(person)).thenReturn(person);

        assertThatThrownBy(() -> {
            service.createPerson(person);
        }).isInstanceOf(IllegalPersonObjectDataException.class);
    }

    @Test
    @Description("Trying to create a (otherwise valid) person with a last name with less than 3 characters should throw IllegalPersonObjectDataException.")
    void createPersonTooShortLastName() {
        PersonObject person = initiallyValidPerson();
        person.setLastName("It");

        Mockito.when(repository.save(person)).thenReturn(person);

        assertThatThrownBy(() -> {
            service.createPerson(person);
        }).isInstanceOf(IllegalPersonObjectDataException.class);
    }

    @Test
    @Description("Trying to create a (otherwise valid) person with a first name exceeding 20 characters should throw IllegalPersonObjectDataException.")
    void createPersonTooLongFirstName() {
        PersonObject person = initiallyValidPerson();
        person.setFirstName("FriedhelmMeyerVonDerHeide");

        Mockito.when(repository.save(person)).thenReturn(person);

        assertThatThrownBy(() -> {
            service.createPerson(person);
        }).isInstanceOf(IllegalPersonObjectDataException.class);
    }

    @Test
    @Description("Trying to create a valid person should return this person.")
    void createPersonFine() {
        PersonObject person = initiallyValidPerson();

        Mockito.when(repository.save(person)).thenReturn(person);

        try {
            assertThat(service.createPerson(person)).isEqualTo(person);
        } catch (IllegalPersonObjectDataException | PersonObjectIdAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    private PersonObject initiallyValidPerson() {
        PersonObject person = new PersonObject();
        person.setId(1);
        person.setFirstName("Max");
        person.setLastName("Mustermann");
        person.setDateOfBirth(LocalDate.of(2000, 1, 1));
        return person;
    }
}
package bous.philipp.datarestpractice.person;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
class PersonObjectServiceTestConfiguration {
    @Bean
    PersonObjectService service() {
        return new PersonObjectService();
    }
}

package bous.philipp.datarestpractice.person;

class PersonMismatchException extends Exception {
    PersonMismatchException(String message) {
        super(message);
    }
}

package bous.philipp.datarestpractice.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class PersonObjectService {

    @Autowired
    PersonObjectRepository personObjectRepository;

    public PersonObject createPerson(PersonObject personObject) throws IllegalPersonObjectDataException, PersonObjectIdAlreadyExistsException {
        if (idAlreadyExists(personObject)) {
            throw new PersonObjectIdAlreadyExistsException(personObject);
        }

        if (isValid(personObject)) {
            return personObjectRepository.save(personObject);
        } else {
            throw new IllegalPersonObjectDataException(personObject);
        }
    }

    public PersonObject update(PersonObject personObject) throws PersonObjectNotFoundException, IllegalPersonObjectDataException {
        PersonObject personInDB = getPersonObject(personObject);

        if (isValid(personObject)) {
            merge(personInDB, personObject);
        } else {
            throw new IllegalPersonObjectDataException(personObject);
        }

        return personObjectRepository.save(personInDB);
    }

    public void delete(PersonObject personObject) throws PersonObjectNotFoundException, PersonMismatchException {
        PersonObject personInDB = getPersonObject(personObject);
        if (personInDB.equals(personObject)) {
            personObjectRepository.deleteById(personObject.getId());
            return;
        }
        throw new PersonMismatchException("You tried to delete a person from " +
                "the DB for which no exact match could be found.");
    }

    private PersonObject getPersonObject(PersonObject personObject) throws PersonObjectNotFoundException {
        return personObjectRepository.findById(personObject.getId())
                .orElseThrow(
                        () -> new PersonObjectNotFoundException(personObject)
                );
    }

    private boolean idAlreadyExists(PersonObject personObject) {
        return personObjectRepository.existsById(personObject.getId());
    }

    private boolean isValid(PersonObject personObject) {
        return satisfiesDateOfBirthConstraints(personObject)
                && satisfiesFirstNameConstraints(personObject)
                && satisfiesLastNameConstraints(personObject);
    }

    private boolean satisfiesDateOfBirthConstraints(PersonObject personObject) {
        return personObject.getDateOfBirth().isBefore(LocalDate.now());
    }

    private boolean satisfiesFirstNameConstraints(PersonObject personObject) {
        return personObject.getFirstName().length() <= 20;
    }

    private boolean satisfiesLastNameConstraints(PersonObject personObject) {
        int length = personObject.getLastName().length();
        return length >= 3 && length <= 20;
    }

    private void merge(PersonObject target, PersonObject source) {
        target.setDateOfBirth(source.getDateOfBirth());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
    }
}

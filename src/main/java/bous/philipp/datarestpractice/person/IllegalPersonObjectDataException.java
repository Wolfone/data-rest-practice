package bous.philipp.datarestpractice.person;

class IllegalPersonObjectDataException extends Exception {
    IllegalPersonObjectDataException(PersonObject personObject) {
        super("PersonObject-constraint violated; " +
                "first name must have 0<= #chars <= 20, " +
                "last name must have 3 <= #chars <= 20, " +
                "date of birth may not be in the future. " +
                "You tried to persist: " + personObject);
    }
}

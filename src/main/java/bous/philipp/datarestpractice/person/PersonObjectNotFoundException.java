package bous.philipp.datarestpractice.person;

class PersonObjectNotFoundException extends Exception{
    public PersonObjectNotFoundException(PersonObject personObject) {
        super("PersonObject with id " + personObject.getId() + " could not be found in the DB.");
    }
}

package bous.philipp.datarestpractice.person;

class PersonObjectIdAlreadyExistsException extends Exception {
    PersonObjectIdAlreadyExistsException(PersonObject personObject) {
        super("You tried to create a person specifying an already existing id (was: " + personObject.getId() + ").");
    }
}

package bous.philipp.datarestpractice.person;

import org.springframework.data.repository.CrudRepository;

interface PersonObjectRepository extends CrudRepository<PersonObject, Long> {
}

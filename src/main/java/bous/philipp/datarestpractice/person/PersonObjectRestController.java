package bous.philipp.datarestpractice.person;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
class PersonObjectRestController {

    private final Logger logger = LoggerFactory.getLogger(PersonObjectRestController.class);

    @Autowired
    PersonObjectService personObjectService;

    @PostMapping("/person/create")
    ResponseEntity<PersonObject> createNewPersonObject(@RequestBody PersonObject personObject) {
        PersonObject newPerson;

        try {
            newPerson = personObjectService.createPerson(personObject);
        } catch (IllegalPersonObjectDataException | PersonObjectIdAlreadyExistsException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(newPerson, HttpStatus.OK);
    }

    @PutMapping("/person/update")
    ResponseEntity<PersonObject> updatePerson(@RequestBody PersonObject personObject) {
        PersonObject updatedPerson;

        try {
            updatedPerson = personObjectService.update(personObject);
        } catch (PersonObjectNotFoundException | IllegalPersonObjectDataException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(updatedPerson, HttpStatus.OK);
    }

    @DeleteMapping("/person/delete")
    ResponseEntity<String> deletePerson(@RequestBody PersonObject personObject) {
        try {
            personObjectService.delete(personObject);
        } catch (PersonObjectNotFoundException | PersonMismatchException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<String>("", HttpStatus.OK);
    }
}

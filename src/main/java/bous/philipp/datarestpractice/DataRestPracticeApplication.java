package bous.philipp.datarestpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataRestPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataRestPracticeApplication.class, args);
	}

}
